/* 
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1(the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis,WITHOUT WARRANTY OF ANY KIND,either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * Alternatively,the contents of this file may be used under the terms
 * of the GNU General Public License(the "GPL"),in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License,indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above,a recipient may use your
 * version of this file under either the License or the GPL.
 *
 * Author Vlad Seryakov vlad@crystalballinc.com
 * 
 */

/*
 * nssavi.c -- Interface to Sophos SAVI library
 *
 *  ns_savi usage:
 *
 *    ns_savi sweepfile path
 *      checks file for virues 
 *      returns list with detected virus names
 *
 *    ns_savi sweepbuffer string
 *      checks string buffer for viruses
 *
 */

#include "savi.h"

static int SAVIInterpInit(Tcl_Interp * interp, void *context);
static int SAVICmd(void *context, Tcl_Interp * interp, int objc, Tcl_Obj * CONST objv[]);

NS_EXPORT int Ns_ModuleVersion = 1;

/*
 *----------------------------------------------------------------------
 *
 * Ns_ModuleInit --
 *
 *	Load the config parameters, setup the structures
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	None
 *
 *----------------------------------------------------------------------
 */

NS_EXPORT int Ns_ModuleInit(char *server, char *module)
{
    char *path;

    path = Ns_ConfigGetPath(server, module, NULL);
    if (Ns_SaviInit() != TCL_OK)
        return TCL_ERROR;
    Ns_TclRegisterTrace(server, SAVIInterpInit, 0, NS_TCL_TRACE_CREATE);
    return NS_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * SAVIInterpInit --
 *
 *      Add ns_savi commands to interp.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */
static int SAVIInterpInit(Tcl_Interp * interp, void *context)
{

    Tcl_CreateObjCommand(interp, "ns_savi", SAVICmd, context, NULL);
    return NS_OK;
}

/*
 *----------------------------------------------------------------------
 *
 * SAVICmd --
 *
 *      SAVI virus detection interface command
 *
 * Results:
 *  	Standard Tcl result.
 *
 * Side effects:
 *  	None.
 *
 *----------------------------------------------------------------------
 */

static int SAVICmd(void *context, Tcl_Interp * interp, int objc, Tcl_Obj * CONST objv[])
{
    int cmd;
    enum commands {
        cmdSweepFile,
        cmdSweepBuffer
    };

    static const char *sCmd[] = {
        "sweepfile",
        "sweepbuffer",
        0
    };

    if (objc < 2) {
        Tcl_AppendResult(interp, "wrong # args: should be ns_savi command ?args ...?", 0);
        return TCL_ERROR;
    }
    if (Tcl_GetIndexFromObj(interp, objv[1], sCmd, "command", TCL_EXACT, (int *) &cmd) != TCL_OK)
        return TCL_ERROR;

    switch (cmd) {
    case cmdSweepFile:
        return Ns_SaviSweep(interp, Tcl_GetString(objv[2]), 0, 0);
    case cmdSweepBuffer:
        return Ns_SaviSweep(interp, Tcl_GetString(objv[2]), Tcl_GetCharLength(objv[2]), 0);
    }
    return TCL_OK;
}
