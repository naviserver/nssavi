ifndef NAVISERVER
    NAVISERVER  = /usr/local/ns
endif

#
# Module name
#
MOD      =  nssavi.so

#
# Objects to build.
#
OBJS     = nssavi.o savi.o

CFLAGS	 = -I../savi_dtk/sav_if -I/usr/local/include/sav_if
MODLIBS	 = -L/usr/local/lib -lsavi

include  $(NAVISERVER)/include/Makefile.module

