/* 
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1(the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis,WITHOUT WARRANTY OF ANY KIND,either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * Alternatively,the contents of this file may be used under the terms
 * of the GNU General Public License(the "GPL"),in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License,indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above,a recipient may use your
 * version of this file under either the License or the GPL.
 *
 * Author Vlad Seryakov vlad@crystalballinc.com
 * 
 */

#include "savi.h"

int Ns_SaviInit()
{
    HRESULT hr;
    CISavi3 *pSAVI;
    U32 version;
    SYSTEMTIME vdlDate;
    U32 detectableViruses;
    OLECHAR versionString[81];
    CISweepClassFactory2 *pFactory;

    // Initialize fake handler to keep all virus data in the memory
    if ((hr =
         DllGetClassObject((REFIID) & SOPHOS_CLASSID_SAVI, (REFIID) & SOPHOS_IID_CLASSFACTORY2, (void **) &pFactory)) < 0) {
        Ns_Log(Error, "nssavi: Failed to get class factory interface: %x", hr);
        return NS_ERROR;
    }
    if ((hr = pFactory->pVtbl->CreateInstance(pFactory, NULL, (REFIID) & SOPHOS_IID_SAVI3, (void **) &pSAVI)) < 0) {
        pFactory->pVtbl->Release(pFactory);
        Ns_Log(Error, "nssavi: Failed to get a CSAVI3 interface: %x", hr);
        return NS_ERROR;
    }
    pFactory->pVtbl->Release(pFactory);
    if ((hr = pSAVI->pVtbl->InitialiseWithMoniker(pSAVI, "ns_savi")) < 0) {
        Ns_Log(Error, "nssavi: Failed to initialise SAVI: %x", hr);
        pSAVI->pVtbl->Release(pSAVI);
        return NS_ERROR;
    }
    if ((hr = pSAVI->pVtbl->LoadVirusData(pSAVI)) < 0) {
        Ns_Log(Error, "nssavi: Unable to load virus data: %x", hr);
        pSAVI->pVtbl->Terminate(pSAVI);
        pSAVI->pVtbl->Release(pSAVI);
        return NS_ERROR;
    }
    // Engine version
    if (pSAVI->pVtbl->GetVirusEngineVersion(pSAVI,
                                            &version,
                                            versionString,
                                            80,
                                            &vdlDate,
                                            &detectableViruses, NULL, (REFIID) & SOPHOS_IID_ENUM_IDEDETAILS, NULL) >= 0)
        Ns_Log(Notice,
               "nssavi: started, %s, Engine version %d.%d %s, Number of detectable viruses: %u, Date of virus data: %d/%d/%d",
               NSSAVI_VERSION, (int) version >> 16, (int) version & 0x0000FFFF, versionString, (unsigned) detectableViruses,
               vdlDate.wMonth, vdlDate.wDay, vdlDate.wYear);
    return NS_OK;
}

int Ns_SaviSweep(Tcl_Interp * interp, char *data, int datalen, char *location)
{
    HRESULT hr;
    char buf[81];
    CISavi3 *pSAVI;
    unsigned long virusType;
    unsigned long pcFetched;
    CISweepResults *pResults = 0;
    unsigned long isDisinfectable;
    CISweepClassFactory2 *pFactory;
    CIEnumSweepResults *pEnumResults;

    Tcl_ResetResult(interp);

    if (!location)
        location = datalen ? "buffer" : data;

    if ((hr =
         DllGetClassObject((REFIID) & SOPHOS_CLASSID_SAVI, (REFIID) & SOPHOS_IID_CLASSFACTORY2, (void **) &pFactory)) < 0) {
        sprintf(buf, "%lx", hr);
        Tcl_AppendResult(interp, "nssavi: %s: Failed to get class factory interface: %s", location, buf, 0);
        return TCL_ERROR;
    }
    hr = pFactory->pVtbl->CreateInstance(pFactory, NULL, (REFIID) & SOPHOS_IID_SAVI3, (void **) &pSAVI);
    pFactory->pVtbl->Release(pFactory);
    if (hr < 0) {
        sprintf(buf, "%lx", hr);
        Tcl_AppendResult(interp, "nssavi: %s: Failed to get a CSAVI3 interface: %s", location, buf, 0);
        return TCL_ERROR;
    }
    if ((hr = pSAVI->pVtbl->InitialiseWithMoniker(pSAVI, "ns_savi")) < 0) {
        sprintf(buf, "%lx", hr);
        Tcl_AppendResult(interp, "nssavi: %s: Failed to initialize SAVI: %s", location, buf, 0);
        pSAVI->pVtbl->Release(pSAVI);
        return TCL_ERROR;
    }
    if (datalen)
        hr = pSAVI->pVtbl->SweepBuffer(pSAVI, location, datalen, data, (REFIID) & SOPHOS_IID_ENUM_SWEEPRESULTS,
                                       (void **) &pEnumResults);
    else
        hr = pSAVI->pVtbl->SweepFile(pSAVI, data, (REFIID) & SOPHOS_IID_ENUM_SWEEPRESULTS, (void **) &pEnumResults);
    if (hr < 0) {
        sprintf(buf, "%lx", hr);
        Tcl_AppendResult(interp, "nssavi: %s: Unable to sweep: %s", location, buf, 0);
        pSAVI->pVtbl->Terminate(pSAVI);
        pSAVI->pVtbl->Release(pSAVI);
        return TCL_ERROR;
    }
    if ((hr = pEnumResults->pVtbl->Reset(pEnumResults)) < 0) {
        sprintf(buf, "%lx", hr);
        Tcl_AppendResult(interp, "nssavi: %s: Failed to reset results enumerator: %s", location, buf, 0);
        pSAVI->pVtbl->Terminate(pSAVI);
        pSAVI->pVtbl->Release(pSAVI);
        return TCL_ERROR;
    }
    while (pEnumResults->pVtbl->Next(pEnumResults, 1, (void **) &pResults, &pcFetched) == SOPHOS_S_OK) {
        if (pResults->pVtbl->GetVirusType(pResults, &virusType) < 0 || virusType == SOPHOS_NO_VIRUS)
            break;
        switch (virusType) {
        case SOPHOS_VIRUS:
            Tcl_AppendResult(interp, "Type=Virus; ", 0);
            break;
        case SOPHOS_VIRUS_IDENTITY:
            Tcl_AppendResult(interp, "Type=Identity; ", 0);
            break;
        case SOPHOS_VIRUS_PATTERN:
            Tcl_AppendResult(interp, "Type=Pattern; ", 0);
            break;
        }
        if ((hr = pResults->pVtbl->GetLocationInformation(pResults, 80, buf, NULL)) >= 0)
            Tcl_AppendResult(interp, "Location=", buf, "; ", 0);
        if ((hr = pResults->pVtbl->GetVirusName(pResults, 80, buf, NULL)) >= 0)
            Tcl_AppendResult(interp, "Name=", buf, "; ", 0);
        if ((hr = pResults->pVtbl->IsDisinfectable(pResults, &isDisinfectable)) >= 0)
            Tcl_AppendResult(interp, isDisinfectable ? "Repair=Yes;" : "Repair=No; ", 0);
        pResults->pVtbl->Release(pResults);
        pResults = 0;
    }
    if (pResults)
        pResults->pVtbl->Release(pResults);
    pEnumResults->pVtbl->Release(pEnumResults);
    pSAVI->pVtbl->Terminate(pSAVI);
    pSAVI->pVtbl->Release(pSAVI);
    return TCL_OK;
}
